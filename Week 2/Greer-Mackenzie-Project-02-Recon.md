## Step 1:

*What is the name of the organization you chose?* cURL

*What do they do?* Open source software that is used in command lines or scripts to transfer data from cars to tvs, routers, audio equipment, phones and more.

*What operating systems do they use on their web server?* Apache/*IX
Why? It is open source, most popular web server OS so it is constantly being updated AND it is easily customizable to a company's specific needs and requirements

*What web server are they using (Apache, IIS, etc.)?* Apache

*What version is it?* 2.4.18

*Does it appear they are hosting their own web server?* No, they are using Online SAS

*What programming languages are used on the site?* *IX, SQL, html

*What are the networks in use by the organization? List Ranges?*  
212.219.56.184-185  
212.219.56.178-179  
128.199.160.6  
178.128.18.249  
128.199.166.85  
178.32.124.167  
62.210.56.221  
34.203.38.235  

*Does it appear they are hosting any other services from their network ranges?* Amazon AWS, Digital Ocean, Online SAS, Google Cloud, TwistedWeb httpd

*What type of information did you turn up using search engines?* Home page, wiki, GitHub, versions, walkthroughs, manuals, troubleshooting

## Step 2:

*Identify key employees.  Get names, positions, salary, phone #, and e-mail addresses.*
Daniel Stenberg (Benevolent Dictator for Life of cURL and Developer for Mozilla), Software Engineer: ~$70,000 USD conversion on avg. probably more with experience and multiple projects ongoing
Bjorn Stenberg Server Admin 
Linus Feltzing Server Admin 

*Do they participate in any professional organizations?* Mozilla, ENEA software, Haxx, cURL project, wolfSSL, Intel, Cavena, Wind River, IBM

*Do they participate in any professional social media sites?* LinkedIN, Twitter

*Is anyone looking for a job?* Not that I can see

*Can you locate interesting corporate documentation, passwords, etc...?* No Passwords, only documentation on how to use the tool, project docs, and manual

*Does your target company have any associations with other companies? e.g.partners* Officially no, other than Daniel Stenberg and his multitude of associations. cURL is opensourced so could be used by many different companies/orgs

Enumerate your targets Domain Name.  Document all additional IP addresses that you have discovered. (Add them to your current list)
Use theharvester, available in your Kali Linux virtual machine, to search your company's domain, e-mail, social media, etc....
save the email address into a text file.
save the hosts into a text file.
help:
https://github.com/laramies/theHarvester (Links to an external site.)Links to an external site.
http://www.edge-security.com/theharvester.php (Links to an external site.)Links to an external site. 
Create a visual map of your selected target's discovered systems.  Identify network address ranges, possible target systems and their purpose, routers, switches, etc......  Is this their DMZ?

I wasn't comfortable actively scanning a spot without any sort of documentation, agreement, or permission and decided against doing so since I am not 100% sure as to how to do so without causing issues.

*Document your advanced Google search strings and their results.*

site:https://curl.haxx.se "daniel stenberg" - interview, governance page
Daniel Stenberg curl - linkedIN, twitter, cURL, wolfSSL
site:https://curl.haxx.se "key members" - governance page
site:https://curl.haxx.se "president" - no results
site:https://curl.haxx.se filetype:txt password - how to search for passwords
site:https://curl.haxx.se documentation - manuals
site:https://curl.haxx.se "partnered with" - not much, got more on this by researching Daniel Stenberg

## Step 3

Physical layout of the company.For all I know their basement/house
Security doors, guards, cameras, etc. I'm guessing standard home security
Badges? No
Vehicle passes? No
Web Cams? Possible
Digital dumpster diving. - If look harder/scanning probably see a huge digital footprint considering all the tools found on Shodan
How does the typical employee dress?  Dress code? Business Professional/Casual
At the end of your paper answer the following questions:

Is there anything that you found particularly useful or juicy during your second phase of your information gathering exercise? How awesome Shodan is, and Recon-NG is a nice tool once you get to understand it
What tools and web sites did you use during this lab exercise? Google, Bing, Shodan, Recon-NG a little, Glassdoor, LinkedIN, cURL