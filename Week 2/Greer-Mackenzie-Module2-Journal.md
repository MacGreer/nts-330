### Module 2 Journal

15:56 - Choose cURL from HackerOne  
15:58 - Opened Shodan  
15:59 - Searched curl.haxx.se  
16:00 - Explored IP addresses in Shodan Search  As I explored I answered questions from Project Recon  
16:30 - Shifted from Shodan to Google dorking, search, LinkedIn, and Glassdoor  
16:42 - Explored ReconNG how to's  
16:55 - Explored VIM RPG until prompted to pay  
17:30 - Did some testing on ReconNG to see how commands worked, what I could explore  
and also looked for different ways to access information through it.  
18:00 - Signed Out  