## Part 1 - BurpSuite  

Getting BurpSuite to run wasn't a problem, even in conjunction with Foxy Proxy. It seemed straightforward to me.  
I created the rule as it stated in the assignment but quite frankly for the assignment, it did nothing for me. I had  
to create a couple of other rules so I wasn't prompted every few seconds. I just right clicked, don't intercept from this  
directory. When I tried going to the enumeration site the first rule helped a little bit. I wasn't sure exactly what I was looking  
at per say.  There weren't any errors that created vulnerabilities and I didn't see any sensitive information in plain text.  I can  
definitely see how this is a powerful tool especially when used together.  

There are so many tools on BurpSuite and i'm not exactly sure how to utilize them. It will have to take some trial and error and a good share  
of google. I tried to utilize the decoder for one of the sections in WebGoat but couldn't seem to figure it out. Not sure if I was trying to  
do too much in a little amount of time and my VM wasn't keeping up or what.  

## Part 2 - WebGoat

#### Intro
---
I enjoyed this web application. I didn't get nearly as far as I wanted to but it is my goal to keep working on it until I have been able  
to complete all of it. I've set my goal to end of summer since I still have two classes after this. It is nice to have a way to apply the  
teachings and skills we are supposedely learning. There is a Cyber Warfare range in Tucson that I want to utilize and I think I will finally  
sit down and figure out how to gain access to the one at school. I got bit by the Cyber bug and i'm all sorts of itchy.  

#### Misconfiguration...
---
I spent a good three hours trying to initially do WebGoat. I had installed (now I see in hindsight) an update image instead of the actual application.  
Things were formatted oddly, I could see somethings and not others. I spent time looking up videos to see why mine looked differently and even got through  
a good amount (or what I though was a good amount) of the application until I realized nope, downloaded the wrong file. So I went back to github and downloaded  
the most updated version of WebGoat and spent more time trying to remember how I opened the jar file in the first place. Once in and all set things finally made sense!  

#### SQL & JS
---
Then I ran into SQL injection and realized I don't know anything about SQL other than it is a way of storing information in a database.  
So I spent another hour or so teaching myself basic SQL and then went back at it. I finally hit my head against a wall and wasn't sure if I had the command wrong or the wrong  
entry field, or quite frankly started to wonder if there was a bug on the lesson. Either way I moved on.  

I then kept going as far as I could into the password reset. I was able to finish this and was absolutely astounded at how easy it can be. Then the next was of  
course Javascript.  

I know a tiny bit about javascript. Quite frankly my eyes and brain were done trying to stare at a screen at this point since I had spent a good eight hours straight  
at this point. So I did as much as I could and when I hit a hurdle instead of learning JS and moving forward this time I decided this was my stopping point.  I figured I'd  
come back to it today and finish as much as I could.  I once again hit a wall where I found what they were looking for but wasn't sure how they wanted it input. I tried  
numerous different ways and it wasn't clicking. So I decided to end there and write up the report.  

#### Continue Learning
---
This has been eye opening and it has given me steps to take on what to learn further on my own as I go through school. SQL, PHP, Javascipt are now on the learn list with  
Python and Ruby.  I also want to refresh myself on HTML and CSS since so much information can be trapped in it. Linux is becoming more of a thing I want to just take and  
replace all of my Windows machines with. It's faster, I can do everything I always wanted to do with my system and couldn't with Windows. I always want the _next_ thing to  
learn and I finally feel like I choose the right career path for that and my thirst for learning and accomplishment is being fueled by all this information I am currently  
_downloading_.  

##### Images  
https://gitlab.com/MacGreer/nts-330/tree/master/Week%204/Images