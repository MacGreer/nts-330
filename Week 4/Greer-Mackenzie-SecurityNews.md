## Cybersecurity: Misconfigured Servers  
>>> 
Source: Palmer, D. (2019, May 30). Cybersecurity: The number of files exposed on misconfigured servers, storage and cloud services has risen to 2.3 billion. Retrieved May 30, 2019, from https://www.zdnet.com/article/cybersecurity-the-number-of-files-exposed-on-misconfigured-servers-storage-and-cloud-services-has-risen-to-2-3-billion/
>>>
### Intro

Everything I have learned up to this point in my education background, food background,  
and technology background, is that if you misconfigure something, it will not do what you  
want it to do or even work period. So many services now from Azure to AWS to any and all  
cloud hosting service is becoming mainstream. It isn't convenient to lug around a hard drive  
or flash drive anymore. It is so much easier to put it up into the cloud and oh wait, we should  
secure that area and then do that. Well too late for that lets just work reactively.  Such is the  
story of modern day computers from day one. Even in today's world availability is a higher  
priority over security. Why? Simply because you can correlate availability with profit/sales/money  
and you can't do that with security.  

### Misconfiguration

Misconfiguring your hardware, software, and firmware can easily leave you open to any number of  
attacks. Even doing a basic setup is risky. Leaving settings such as logins or WPS for instance  
is dangerous for home users and those setups are basic. You just have to change the username or password.  
Then we expect people to take newer technology associated with a cloud they don't even comprehend what  
is and expect them to configure them correclty or at least safely. You know those block sorters for kids?  
Misconfiguration is like putting a circle in the square option. Does it fit? Yes, but it leaves gaps. Those  
gaps are what malicous users look for and prey upon.  

### Experience

In my very limited experience dealing with the 'average' user who is a glorified mom and pop, is that they will  
literally do what is easy. They can follow a prompt sure, but they will do what is convenient and easy for them  
so that they can concentrate on other items on their agenda. P@ssword85 is easy to remember and it doesn't get  
flagged by the auto prompt of it isn't secure enough. But in reality, this is far away from a safe password. People  
don't necessarily hire other people for two main reasons. One, they can't afford too/don't want to spend the money,  
or two, they don't trust a 3rd party to handle their information.  

### Conclusion

As the cloud becomes more mainstream and easier to access with the internet of things in full swing we will see this  
trend continue upwards until certain security measures are required. At this point we are behind and it becomes yet  
another 'tech race' to leap frog each other into the future... once again. My hope is to help move the world into a  
proactive state rather than a reactive. 