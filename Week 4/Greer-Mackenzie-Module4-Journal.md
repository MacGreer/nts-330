## Journal 4.1

06/01/2019
0530 - Configured BurpSuite  
0535 - Downloaded FoxyProxy  
0540 - FoxyProxy Configured  
0545 - Downloaded WebGoat  
0546 - Error couldn't load  
0547 - Played with settings and tried running different commands  
0550 - Error again, no access  
0600 - Discovered it was case sensitive so ran command again and worked  
0608 - Loaded WebGoat and logged in (without creating user... odd)  
0615 - Trying to do labs and lessons... layout is odd and incomplete?  
0830 - Well discovered that the version I downloaded was an update image...  
0833 - Downloaded full server version and latest update  
0836 - Created user... now this makes sense!  
0845 - Started Intro  
0848 - Started General  
0945 - Finished General and the exercises  
1000 - Started SQL and man... I don't have any idea what is going on with SQL  
1015 - I'm not understanding what I am saying in SQL  
1020 - Stopped lab and started to learn SQL 101 @ https://www.w3schools.com/SQl/default.asp  
1200 - I feel somewhat better about SQL and what I need to accomplish  
1228 - Completed the first lab after grabbing lunch and MAN I get this way better than previously  
1230 - 2nd lab done while eating  
1232 - 3rd lab done  
1233 - 4th lab done  
1235 - 5th lab done  
1240 - 9th page done  
1250 - 10th page done  
1305 - 11th page done  
1400 - Can't seem to get the 12th and 13th to work. Not sure if it is speficic syntax,  
       Or if it is some bug on the application or what.  
1430 - Moved on to the password challenges and was surprised at how easy these were...  
       I get these are meant to be easier but still, scary.  
1500 - Tried going into cross scripting and DOMs... got lost and frustrated after a day of back  
       and forth. Decided to step away and spend time with Family.  

06/02/2019  
0700 - Tried picking up the cross scripting again and got past the first two exercises and then got  
       MAJORLY stuck. Found the right path/route i'm sure but it wasn't taking it  
0800 - Organized pictures taken of completetion and then started doing the write ups  
1000 - Spent time with fam for breakfast, now catching up on final touches for homework
