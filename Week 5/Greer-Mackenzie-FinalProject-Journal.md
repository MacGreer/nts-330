## HackTheBox Journal

0744: Started project, navigated to www.hackthebox.eu  
0745: Searched in inspector for clues, saw a code from burpsuite and tried it - failed  
0746: Went to Network to look @ post request for the previous failure and tried to edit the request - failed  
0755: Kept searching network and such but couldn't find anything. Tried page source but it was one long line so went back to Inspector for easier view  
0800: Looked at console, nice pic, went to Network again and filtered HTML - nothing JS - script for invite.api.min.js  
0802: Tried putting that into console, nothing
0803: Went to script in inspector  
0805: Realized it had been right there in front of my face the ENTIRE time  
0806: Copied the url source for the js  
0807: Put it in and saw an output of mostly gibberish until the end. Need to create a Post request, saw also 'verifyInivteCode and makeInviteCode'  
0809: Googled verify and make to see what that was... javascript... doh js request  
0810: put makeInviteCode into console - nothing?
0811: put makeInviteCode() into console and got a code which did not work on the invite page  
0812: Then saw the string of values was encoded with base64 (doh, need to read the whole thing)  
0813: Googled how to decode in terminal if possible  
0815: It is possible and tried to find the right terminology/commands to make it do so  
0820: Finally got it  and directions to make a POST request to the url extension  
0822: Put the extension on the end of the root and nothing...  
0823 Checked the Network tab and realized it was a GET not a POST so I edited and resent  
0825: Succes! the values was there and it said encoded, I assume base64 again  
0826: Went back to terminal to decode and got what looked like an old school looking game key  
0827: Put in code at Invite screen and got in!  

Images uploaded into folder for Week 5  
https://gitlab.com/MacGreer/nts-330/tree/master/Week%205/Final%20Project%20Pictures
