## City of Baltimore Update  
https://mayor.baltimorecity.gov/news/press-releases/2019-05-20-city-provides-update-baltimore-ransomware-attack

### One Month Later  
---

Yup, it has been one month since the City of Baltimore has been taken to its knees at the mercy  
of this ransomware attack. The Mayor himself gave the update and wanted to assure everyone that everything  
was being done to bring systems back up in a secure and safe manner. I have to say for having thousands  
of systems go offline, thousands of employees to go through, working with the FBI, and trying to maintain  
future integrity with security and information they are taking this very well and moving in a professional  
manner. They could have given into the ransomware, they could have just given up and pointed the finger. Instead,  
they rolled up their sleeves and decided to move forward.  

### Moving Forward  
---

Due to ongoing investigations, they are not able to provide specifics and/or details on what occurred, what they  
are currently working on, which systems, etc, but have provided a general plan where they are going to start  
with the more critical systems and if need be rebuild those with security in mind. In my opinion, I think this  
should be standard practice across the board. Availability, accessibility is great, but not at the expense  
of security. Baltimore is doing it right in my mind, they stopped trying to point fingers and they got to work.  

### New Appointment  
---

The Mayor also appointed a new Deputy Chief of Staff Operations who will be providing updates going forward. I give  
my best to this person since this could be the role of a lifetime. It is one thing to do such a job in a fully operational  
infrastructure let alone one that is plagued with ransomware. I tip my hat to her and wish her the best. It is nice to see  
people stepping up and not shying away from positions regardless of the situation.  

### Conclusion  
---

With all the things going on in the United States I just want to give Baltimore a shout out. When the Boston Marathon was  
bombed there was media coverage all over the place. The midwest with their multitude of weather phenomena this year? Coverage  
all over the place. Politics? Always. But I haven't seen a single mention of Baltimore unless I search for it and then I can find  
it. But, my point is I have to search for it. This should be seriously talked about because it isn't just Baltimore. Numerous other  
cities have been attacked as well and people every day are affected. It is time we stop making it a state of fact and start making  
it abundantly clear that it should be taken seriously. It isn't a game. The hypocrisy of the media where they do specials on Network  
Security and personal digital security and yet they gloss over the repercussions or lasting consequences.  

I digress and will step down from my soapbox. Thanks for reading.  

#techrespect #BaltimoreStrong