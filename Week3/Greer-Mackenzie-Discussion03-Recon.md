## Recon
---

### Chainlink
#### https://hackerone.com/chainlink
---
#### Like(s)
---
Project Tracker: Lists where existing bugs are kept so you don't try to overwork yourself getting the same  
vulernability as someone else previously.

Scratchpad: Another list of known issues

Specifically states what is IN scope and what is NOT in scope. 

#### Dislike(s)
---
Some of the Out of scope vulnerabilities section is in my opinion is just plain lame.  
The OS related vulnerabilities, social engineering is prohibited... it just tells me  
they want help 'double checking' their coding. They want a very limited test in my eyes  
since the biggest vulnerability to any product is the user... and then the OS.

### Defect Dojo
#### https://hackerone.com/defectdojo
---
#### Like(s)
---
They don't trim your sails quite as much as others  

#### Dislike(s)
---
Not a lot of information is displayed. Quite frankly there is a reason why nobody has tried  
in my opinion. Everything is just too vague. You aren't exactly sure what is in scope and what is not.  
This feels like a 'trap' almost. Even if it isn't it isn't _worth_ it to any user to attempt any sort of  
testing on this one.