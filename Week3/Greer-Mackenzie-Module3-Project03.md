## Part 1  
https://asciinema.org/a/r3ys4UzjIJvDQvxDQ83TxlW1t  
nmap Commands via Terminal  
#### Questions
---
1. Port scan the hosts in scope with Nmap.
151.101.130.29  
151.101.194.29  
151.101.66.29  
151.101.2.29  
Created a networkhosts.txt file per Question 12 and utilized in pretty much every command going forward  
2. Scan a host adjusting the timing of requests with Nmap.  
Utilized the -T4 option: nmap -T4 -iL networkhosts.txt
3. Use Nmap to sweep the scope for systems running web servers on port 80 and port 443.  
nmap -p80,443 -iL networkhosts.txt
4. Run a scan on a host and tell Nmap to display the reason it finds the port in the state it does.  
nmap -reason -T4 -iL networkhosts.txt  
5. Scan a system with Nmap and output the results to a Normal File.  
command: nmap -oN testsave.nmap -iL networkhosts.txt  
6. Scan a host as if it where denying ICMP (ping).  
nmap -sn -PS -iL networkhosts.txt  
7. Port scan on a host for open ports 1 through 500 with Ncat. Yes, Ncat.  
nc -z -v livestream.com 80  
I couldn't get Ncat to work... it said the command didn't exist. So I went to netcat which to my understanding Ncat is Nmap's  
version of Netcat.  
When do you think you might use Ncat vs Nmap?  
If I want to make a connection with an open port and then to collaborate, send information, utilize a different packet perhaps.  
8. Perform Operating System identification on one of the hosts on your network.  
nmap -O -A -iL networkhosts.txt  
How accurate was the guess by the tool?  
I think it was pretty accurate.  
9. Perform application fingerprinting on a host with Nmap.
Utilized in previous question.  
In your estimation did Nmap properly identify the services running on the machine?  
It showed Varnish for the webhost/accelerator which is usually run on Linux which the scan picked up.  
It even provided the hardware it was utilized on: Actiontec - WAPs  
Where there unknown application fingerprints?  
Not that I saw.  
If Nmap doesn’t know what a service is, what steps could you take to determine what the service is?  
If it doesn't know what it is, it will provide an output that can be utilized to decipher and put up against other services.  
One way of doing this is to email Nmap and you could also post it up on the web to see if anyone can match against another KB.  
10. You are on a penetration test. Your customer asks you to identify all of the hosts in a given network range.  
You notice that they are filtering ICMP so you can’t ping hosts to determine if they are alive.  
How would you determine which hosts in the network range are actually up?  I would utilize -sn & -PS for nmap options.  
-sn: skips the port scan for host discovery  
-PS: Sends an empty TCP packet and will either be denied off the bat (closed port) or it will go to the 2nd stage of the TCP handshake  
and a request will be sent back (meaning port open).  
11. Take a couple of the hosts from your network and put them in a plain text file.  
Put the IP addresses in the file so there is only one per line. Name this file “networkhosts.txt”  
Use Nmap with the appropriate command line argument to import this file and scan the contents.  
Utilized this for a lot of my commands. Made it easier since my target had 4 hosts.

### Part 2
---
#### Questions
---
1. Enter the IP address of the target system into OpenVAS and Scan it.  
127.0.0.1 - Used loopback to scan myself  
2. Create a new custom scan policy in OpenVAS, In this new policy trim down the vulnerability checks so that they are more relevant   
to the operating system you are scanning.  
    - Added a new port list for 'commonly hacked ports'  
    - Created a new target (same loopback IP)  
    - New task/scan with new port list
Give a few examples of checks that you removed.  
    - Changed QoD from 70 down to 30 (Since nothing came up?)
Give a few examples of checks that you kept.  
    - All default  
3. Scan the IP address of the target system again using the new setup that you created.  
Check  
4. Do you see any items you suspect as false positives? Why do you believe them to be false?  
No I don't.  
5. Do you believe that there are vulnerabilities on the system that the vulnerability scanner didn’t find? Why do you believe so?  
I don't believe so since the unit it is scanning is the VM of Kali which has more strict settings in regards to this sort of thing.  
I have a feeling this would be a different story if I did this on my actual computer (which I will do when I have a little free time).  
6. Export the data from the scan in a format you can read later. (PDF is fine)  
Will attach with assignment.